#include <iostream>
void FindEvenOrOdd(int TargetNumber, bool IsOdd)
{
	for (int i = IsOdd; i <= TargetNumber; i += 2)
	{
		std::cout << i << '\n';
	}
}

int main()
{
	int TargetNumber;
	bool IsOdd;

	std::cout << "Vvedite chislo: ";
	std::cin >> TargetNumber;

	std::cout << "Dlya chetnih vvedite 0, dlya nechetnih 1: ";
	std::cin >> IsOdd;

	FindEvenOrOdd(TargetNumber, IsOdd);
}